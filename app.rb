require 'digest/md5'
require 'base64'

require 'sinatra'
require 'shellwords'

#require 'RMagick'

class String
  def is_i?
    self.to_i.to_s == self
  end
end

module Scaffold
  class App < Sinatra::Base

    helpers do
      def plain(body)
        headers 'Content-Type' => 'text/plain'
        body body
      end
    end

    def process_img(size, type)

      max = 3000
      if size.include? 'x'
        width, height = size.split('x')
      else
        width  = size.dup
        height = size.dup
      end

      error 400 unless width.is_i? && height.is_i? && width.to_i <= max && height.to_i <= max

      w = width.to_i
      h = height.to_i

      bgcolor = 'CCCCCC'
      if params.has_key?('bgcolor') && params['bgcolor'].scan(/^[a-f0-9]{6}$/i)
        bgcolor = params['bgcolor'].upcase
      end

      if bgcolor.scan(/#^[a-f0-9]{6}$/i)
        bgcolor = '#' + bgcolor
      end

      textcolor = 'EEEEEE'
      if params.has_key?('textcolor') && params['textcolor'].scan(/^[a-f0-9]{6}$/i)
        textcolor = params['textcolor'].upcase
      end

      if textcolor.scan(/#^[a-f0-9]{6}$/i)
        textcolor = '#' + textcolor
      end

      unless %w(gif png jpg).include? type
        type = 'gif'
      end

      content_type = type == 'jpg' ? 'image/jpeg' : 'image/' + type

      if params.has_key? 'text'
        text = " #{ params['text'] } "
      elsif params.has_key? 'text64'
        text = " #{ Base64.decode64(params['text64']) } "
      else
        text = " #{ w }x#{ h } "
      end

      cmd = "convert -size #{ w }x#{ h } -background '#{ bgcolor }' -fill '#{ textcolor }' -gravity Center label:#{ text.shellescape } #{ type }:-"

      etag = Digest::MD5.hexdigest cmd
      halt 304 if request.env.has_key?('IF_NONE_MATCH') && etag == request.env['IF_NONE_MATCH']

      #  self.background_color = bgcolor
      #mark = Magick::Image.new(w, h) do
      #  self.format = type.upcase
      #  self.caption = text
      #end

      #gc = Magick::Draw.new
      #gc.annotate(mark, 0, 0, 0, 0, text) do
      #  self.gravity = Magick::CenterGravity
      #  self.fill = textcolor
      #end

      #images = Magick::Image.read("caption: #{ text } ") do
      #    self.size = "#{ w }x#{ h }"
      #    self.background_color = bgcolor
      #    self.fill = textcolor
      #    self.gravity = Magick::CenterGravity
      #    self.pointsize = 18
      #end

      # mark = images.first
      # mark.format = type.upcase

      headers \
        'Content-Type'  => content_type,
        'Cache-Control' => 'public, max-age=31536000, s-maxage=31536000',
        'Etag'          => etag
      #body mark.to_blob
      body `#{ cmd }`

      #headers \
      #    'Cache-Control'             => 'public, max-age=31536000, s-maxage=31536000',
      #    'Cache-Control'             => 'public, max-age=31536000, s-maxage=31536000',
      #    'Content-Type'              => obj.content_type,
      #    'Content-Transfer-Encoding' => 'binary',
      #    'Content-Length'            => obj.content_length.to_s,
      #    'Etag'                      => obj.etag,
      #    'Expires'                   => expires.strftime('%a, %d %b %Y %T GMT'),
      #    'Last-Modified'             => obj.last_modified.strftime('%a, %d %b %Y %T GMT'),
      #    'Vary'                      => 'Accept-Encoding'
      #
      #body obj.read
    end

    get '/:size.:format' do
      plain "size = #{ params[:size] }\ntype = #{ params['format'] }"

      process_img params[:size], params[:format]
    end

    get '/:size' do
      plain "size = #{ params[:size] }\ntype = gif"

      process_img params[:size], 'gif'
    end


    get '/?' do
      plain 'Howdy there'
    end

    error 400 do
      plain 'You sent something wrong'
    end

    error 404 do
      plain 'Not found'
    end

    error 500..510 do
      plain 'How embarrassing, something went wrong'
    end

    run! if __FILE__ == $0
  end
end