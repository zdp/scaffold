Encoding.default_external = 'UTF-8' if defined? Encoding
Encoding.default_internal = 'UTF-8' if defined? Encoding

require './app.rb'

run Scaffold::App